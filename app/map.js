
var blendMode = function(evt) {
  evt.context.globalCompositeOperation = 'multiply';
};


module.exports = {
  setup: function(el){

    var baseLayer = new ol.layer.Tile({
      source: new ol.source.XYZ({
        //url: 'https://api.mapbox.com/styles/v1/ckaiser1/cixt9ol53003h2rplh41j6vwb/tiles/256/{z}/{x}/{y}?access_token=pk.eyJ1IjoiY2thaXNlcjEiLCJhIjoiY2l4dDluM2ZkMDAxeDMzbzBlN3BnbmticyJ9.lPIMbXavSK294Loq3zPJtA'
        url: 'https://server.arcgisonline.com/ArcGIS/rest/services/Canvas/World_Light_Gray_Base/MapServer/tile/{z}/{y}/{x}'
      }),
      opacity: 1
    });

    this.map = new ol.Map({
      target: el,
      layers: [baseLayer, ],
      view: new ol.View({
        center: ol.proj.fromLonLat([6.703, 46.545]),
        zoom: 11,
        maxZoom: 14,
        minZoom: 8
      })
    });

    this.overlays = {
      'newpop': { url: 'http://igd.unil.ch/pcw/t/index.php?f=tim{tim}_cit{cit}_mob{mob}_loc{imob}/new_pop{kde}/{z}/{x}/{y}.png' },
      'origpop': { url: 'http://igd.unil.ch/pcw/t/index.php?f=orig_pop{kde}/{z}/{x}/{y}.png' },
      'dpop': { url: 'http://igd.unil.ch/pcw/t/index.php?f=tim{tim}_cit{cit}_mob{mob}_loc{imob}/dpop{kde}/{z}/{x}/{y}.png' },
      'dpop-cit': { url: 'http://igd.unil.ch/pcw/t/index.php?f=tim{tim}_cit{cit}_mob{mob}_loc{imob}/dpop_cit{kde}/{z}/{x}/{y}.png' },
      'dpop-mob': { url: 'http://igd.unil.ch/pcw/t/index.php?f=tim{tim}_cit{cit}_mob{mob}_loc{imob}/dpop_mob{kde}/{z}/{x}/{y}.png' },
      'dpop-imob': { url: 'http://igd.unil.ch/pcw/t/index.php?f=tim{tim}_cit{cit}_mob{mob}_loc{imob}/dpop_loc{kde}/{z}/{x}/{y}.png' },
    };

    this.tim = 0.5; this.cit = 0.6; this.mob = 0.2; this.imob = 0.2; this.kde = false;
    this.layer = 'newpop';
    this._current_overlay = this.build_layer(this.layer)
    this.map.addLayer(this._current_overlay);

    return this;
  },

  build_layer: function(n){
    var url = this.overlays[n].url;
    url = url.replace('{tim}', ('00'+(10*this.tim)).slice(-2));
    url = url.replace('{cit}', ('00'+(10*this.cit)).slice(-2));
    url = url.replace('{mob}', ('00'+(10*this.mob)).slice(-2));
    url = url.replace('{imob}', ('00'+(10*this.imob)).slice(-2));
    url = url.replace('{kde}', (this.kde ? '_kde250' : ''));
    var lyr = new ol.layer.Tile({ source: new ol.source.XYZ({ url: url }) });
    lyr.on('precompose', blendMode);
    lyr.on('postcompose', blendMode);
    return lyr;
  },

  update: function(){
    this.map.removeLayer(this._current_overlay);
    this._current_overlay = this.build_layer(this.layer);
    this.map.addLayer(this._current_overlay);
  }

};


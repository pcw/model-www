const d3 = require('d3');
const hrt3 = Math.sqrt(3) / 2;

function tri2svg(a,b,c){
  return [
    0.5 * ((2*b+c)/(a+b+c)),
    hrt3 * (c / (a+b+c))
  ];
}

function svg2tri(x,y){
  var a = -(100/3)*(3*x + Math.sqrt(3)*y - 3);
  var b = 100*x - ((100*y)/Math.sqrt(3));
  var c = 200*y / Math.sqrt(3);
  return [a,b,c];
}

function range(start, stop, step) {
  var result = [];
  for (var i=start; i<stop+1e-5; i+=step) { result.push(i); }
  return result;
}

module.exports = {
  setup: function(el, resolution, margin){

    var self = this;
    this.cb = [];
    this.a = 0.2, this.b = 0.2, this.c = 0.6;

    resolution = resolution || 0.1;
    margin = margin || 50;

    var svg = d3.select(el),
        width = +svg.attr('width'),
        height = +svg.attr('height');

    // Triangle width and height
    const tw = Math.min(width - margin, height - margin),
          th = tw;

    var triangle = svg.append('g')
      .attr('class', 'triangle')
      .attr('transform', 'translate('+margin/2 + ',' + ((margin/2)+10) + ')');

    triangle.append('path')
      .attr('d', 'M0 '+(th*0.866)+'L'+tw+' '+(th*0.866)+'L'+(tw/2)+' 0Z')
      .attr('fill', '#c7c7c7');

    const steps = range(resolution, 1-resolution, resolution);
    for (var i in steps){
      var s = steps[i];
      triangle.append('line')
        .attr('x1',         tri2svg(0.0,   s, (1-s))[0]*tw)
        .attr('y1', (th*0.866) - (tri2svg(0.0,   s, (1-s))[1]*th))
        .attr('x2',         tri2svg((1-s), s, 0.0)[0]*tw)
        .attr('y2', (th*0.866) - (tri2svg((1-s), s, 0.0)[1]*th))
        .attr('stroke-width', '0.4pt')
        .attr('stroke', '#fff');

      triangle.append('line')
        .attr('x1',         tri2svg(s,   0.0, (1-s))[0]*tw)
        .attr('y1', (th*0.866) - (tri2svg(s,   0.0, (1-s))[1]*th))
        .attr('x2',         tri2svg(s, (1-s), 0.0)[0]*tw)
        .attr('y2', (th*0.866) - (tri2svg(s, (1-s), 0.0)[1]*th))
        .attr('stroke-width', '0.4pt')
        .attr('stroke', '#fff');

      triangle.append('line')
        .attr('x1',         tri2svg(0.0, (1-s), s)[0]*tw)
        .attr('y1', (th*0.866) - (tri2svg(0.0, (1-s), s)[1]*th))
        .attr('x2',         tri2svg((1-s), 0.0, s)[0]*tw)
        .attr('y2', (th*0.866) - (tri2svg((1-s), 0.0, s)[1]*th))
        .attr('stroke-width', '0.4pt')
        .attr('stroke', '#fff');
    }

    var toptxt = triangle.append('text')
      .attr('class', 'value-display value-top')
      .attr('transform', 'translate(' + (+tw/2) + ',' + (-12) + ')')
      .text('Citadinity');

    var lefttxt = triangle.append('text')
      .attr('class', 'value-display value-left')
      .attr('transform', 'translate(' + (-(margin/2)+3) + ',' + (th) + ')')
      .text('Mobility');

    var righttxt = triangle.append('text')
      .attr('class', 'value-display value-right')
      .attr('transform', 'translate(' + (tw+(margin/2)-3) + ',' + (th) + ')')
      .text('Immobility');

    var handle = triangle.append('circle')
      .attr('class', 'handle handle-triangle')
      .attr('r', 7)
      .attr('cx', handlePosition(20,20,60).x)
      .attr('cy', handlePosition(20,20,60).y)
      .call(d3.drag()
        .on('start.interrupt', function(){})
        .on('start drag', function(){ dragHandle(d3.event.x, d3.event.y); })
      );

    var values = triangle.append('g')
      .attr('class', 'triangle-value-display')
      .attr('transform', 'translate(' + (-margin/2) + ',' + (th+30) + ')');

    values.append('text')
      .attr('transform', 'translate(0,0)').text('Citadinity:');
    var valueTop = values.append('text')
      .attr('transform', 'translate(90,0)').text('60%');

    values.append('text')
      .attr('transform', 'translate(0,18)').text('Mobility:');
    var valueLeft = values.append('text')
      .attr('transform', 'translate(90,18)').text('20%');

    values.append('text')
      .attr('transform', 'translate(0,36)').text('Immobility:');
    var valueRight = values.append('text')
      .attr('transform', 'translate(90,36)').text('20%');


    function handlePosition(a,b,c){
      var p = tri2svg(a,b,c);
      return {x: p[0]*tw, y: (th*0.866)-p[1]*th};
    }

    function dragHandle(x,y){
      var p = svg2tri(x/tw, ((th*0.866)-y)/th);
      var a = p[0], b = p[1], c = p[2];
      res = resolution*100;
      a = Math.round(a/res)*res;
      b = Math.round(b/res)*res;
      c = Math.round(c/res)*res;
      if (a+b+c != 100) return;
      if (a<0 || b<0 || c<0) return;
      var np = handlePosition(a,b,c);
      handle.attr('cx', np.x);
      handle.attr('cy', np.y);
      valueTop.text(c+'%');
      valueLeft.text(a+'%');
      valueRight.text(b+'%');

      if (self.a != a || self.b != b || self.c != c){
        self.a = a; self.b = b; self.c = c;
        for (var i=0; i < self.cb.length; i++){
          self.cb[i](c, a, b);
        }
      }
    }

    return this;
  },

  onUpdate: function(fn){
    this.cb.push(fn);
  }
};
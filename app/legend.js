const d3 = require('d3');

module.exports = {

  margin: {top: 20, bottom: 20},

  setup: function(el){
    this._svg = d3.select(el);
    this._caiss = this._svg.append('g').attr('class', 'leg-caissons');
    this._ticks = this._svg.append('g').attr('class', 'leg-ticks');

    this._values = this._svg
      .append('g')
      .attr('class', 'leg-values')
      .attr('font-family', 'Verdana, sans-serif')
      .attr('font-size', 11);

    return this;
  },

  limits: function(l){
    this._limits = l;
    return this;
  },

  colors: function(c){
    this._colors = c;
    return this;
  },

  draw: function(){
    var self = this;
    var nclasses = this._colors.length;
    var h_caiss = (this._svg.attr('height') - this.margin.top - this.margin.bottom) / nclasses;

    this._caiss
      .selectAll('rect')
      .data(this._colors)
      .enter()
      .append('rect')
      .attr('width', 25)
      .attr('stroke', '#000')
      .attr('stroke-width', 0.4);

    this._caiss
      .selectAll('rect')
      .data(this._colors)
      .attr('x', 10)
      .attr('y', function(d, idx){
        return (nclasses-idx-1)*h_caiss + self.margin.top;
      })
      .attr('height', function(d) { return h_caiss-3; })
      .attr('fill', function(d,idx){ console.log('fill', d, idx); return d; });

    this._caiss
      .selectAll('rect')
      .data(this._colors)
      .exit()
      .remove();


    this._ticks
      .selectAll('line')
      .data(this._limits)
      .enter()
      .append('line')
      .attr('stroke', '#000')
      .attr('stroke-width', 0.4);

    this._ticks
      .selectAll('line')
      .data(this._limits)
      .attr('x1', 35)
      .attr('y1', function(d,idx){ return idx*h_caiss + self.margin.top - (idx == 0 ? 0 : 3); })
      .attr('x2', 40)
      .attr('y2', function(d,idx){ return idx*h_caiss + self.margin.top - (idx == 0 ? 0 : 3); });

    this._ticks
      .selectAll('line')
      .data(this._limits)
      .exit()
      .remove();


    this._values
      .selectAll('text')
      .data(this._limits)
      .enter()
      .append('text');

    this._values
      .selectAll('text')
      .data(this._limits)
      .attr('x', 44)
      .attr('y', function(d,idx){ return (nclasses-idx)*h_caiss + self.margin.top - (idx == nclasses ? 0 : 3) + 3; })
      .text(function(d,idx){ return d; });

    this._values
      .selectAll('text')
      .data(this._limits)
      .exit()
      .remove();

    return this;
  }

}
var d3 = require('d3');

module.exports = {
  setup: function(){

    var self = this;
    this.cb = [];
    this.val = 0.5;

    var svg = d3.select('.reduce-car svg'),
        margin = {right: 50, left: 8},
        width = +svg.attr('width') - margin.left - margin.right,
        height = +svg.attr('height');

    var x = d3.scaleLinear()
      .domain([0, 100])
      .range([0, width])
      .clamp(true);

    var slider = svg.append('g')
      .attr('class', 'slider')
      .attr('transform', 'translate('+margin.left + ',' + height/2 + ')');

    slider.append('line')
      .attr('class', 'track')
      .attr('x1', x.range()[0])
      .attr('x2', x.range()[1]);

    var handle = slider.insert('circle', '.track-overlay')
      .attr('class', 'handle')
      .attr('r', 7)
      .attr('cx', x(50))
      .call(d3.drag()
        .on('start.interrupt', function(){ slider.interrupt(); })
        .on('start drag', function(){ reduceCar(x.invert(d3.event.x)); })
      );

    var txt = svg.append('text')
      .attr('class', 'value-display')
      .attr('transform', 'translate('+ (+svg.attr('width')-2) + ',' + ((height / 2) + 3) + ')')
      .text('50%');

    function reduceCar(h){
      h = parseInt(h/10)*10;
      handle.attr('cx', x(h));
      txt.text(parseInt(h)+'%')
      var v = h / 100;
      if (v != self.val){
        self.val = v;
        for (var i=0; i < self.cb.length; i++){
          self.cb[i](v);
        }
      }
    }

    return this;
  },

  onUpdate: function(fn){
    this.cb.push(fn);
  }
};
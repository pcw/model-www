'use strict';

var ReduceCarControl = require('./app/reduce-car-control');
var AdaptationBehaviourControl = require('./app/adaptation-behaviour-control');
var PopulationMap = require('./app/map');
var Legend = require('./app/legend');


var leg_pop = {
  limits: ['1', '2', '5', '10', '15', '20', '30', '50', '100', '5000'],
  colors: ['#ffffcc','#ffeda0','#fed976','#feb24c','#fd8d3c','#fc4e2a','#e31a1c','#bd0026','#800026']
};
var leg_dpop = {
  limits: ['-500', '-20', '-10', '-5', '-2', '-1', '1', '2', '5', '10', '20', '500'],
  colors: ['#045a8d', '#2b8cbe', '#74a9cf', '#bdc9e1', '#f1eef6', '#ffffff', '#fee5d9','#fcae91','#fb6a4a','#de2d26','#a50f15']
};



var legends = {
  'newpop': leg_pop,
  'origpop': leg_pop,
  'dpop': leg_dpop,
  'dpop-cit': leg_dpop,
  'dpop-mob': leg_dpop,
  'dpop-imob': leg_dpop
};

document.addEventListener('DOMContentLoaded', function(){
  var reducar = ReduceCarControl.setup();
  var adbeh = AdaptationBehaviourControl.setup('.adaptation-behaviour svg', 0.2, 50);
  var popmap = PopulationMap.setup('pcw-map');

  var leg = Legend.setup('.legend svg')
                  .limits(leg_pop.limits)
                  .colors(leg_pop.colors)
                  .draw();

  $('#kde').on('click', function(e){
    popmap.kde = $('#kde')[0].checked;
    popmap.update();
  });

  $('#layer').on('change', function(e){
    popmap.layer = $('#layer').val();
    popmap.update();
    var l = legends[popmap.layer];
    console.log(popmap.layer, l)
    leg
      .limits(l.limits)
      .colors(l.colors)
      .draw();
  });

  reducar.onUpdate(function(tim){
    popmap.tim = tim;
    popmap.update();
  });

  adbeh.onUpdate(function(cit, mob, loc){
    popmap.cit = cit / 100; popmap.mob = mob / 100; popmap.imob = loc / 100;
    popmap.update();
  });
});


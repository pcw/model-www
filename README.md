# PCW interactive model Web app

This is the interactive model for the PostCarWorld project.

The project is a simple Webpack project. To run in dev mode, install the NPM packages:

```bash
npm install
```

and then launch the Webpack dev server:

```bash
./node_modules/.bin/webpack-dev-server
```

from within the project directory.

